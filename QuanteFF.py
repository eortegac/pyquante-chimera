from MMTK import ParticleScalar
import numpy as np
from MMTK.ForceFields.ForceField import ForceField, EnergyTerm
from MMTK import ParticleVector, SymmetricPairTensor
from Scientific.Geometry import delta
import PyQuante 
from PyQuante import hartree_fock
from PyQuante.NumWrap import array
import numpy as np
from Scientific.Geometry import Vector
#
# Each force field term requires two classes. One represents the
# abstract force field (HarmonicOscillator in this example), it knows
# nothing about any molecular system to which it might be applied.
# The other one ( QuanteTerm ) stores all the parameters required
# for energy evaluation, and provides the actual code.
#
# When a force field is evaluated for the first time for a given universe,
# the method evaluatorTerms() of the force field is called. It creates
# the EnergyTerm objects. The method evaluate() of the energy term object
# is called for every energy evaluation. Consequently, as much of the
# computation as possible should be done outside of this method.
#

###########
#
# Needed to call the NumericEnergyForcesHF from Minimizers in 
# order to get all the info (gradients and energy, in that order)
#
##############
class  QuanteTerm (EnergyTerm):

    # The __init__ method only remembers parameters. Note that
    # EnergyTerm.__init__ takes care of storing the name and the
    # universe object.
    def __init__(self, universe, atoms_quante):
        EnergyTerm.__init__(self, 'quante_term', universe)
        self.atoms_quante = atoms_quante

    # PyQuante stuff provided by Minimizers.py

    def shift_geo(self,atomlist,atom,dir,amount):
        atomlist[atom].r[dir] += amount
        return atomlist

    def RHFEnergyFunction(self,atomlist,**kwargs):
        from PyQuante.hartree_fock import rhf
        return rhf(atomlist,**kwargs)[0]

    def NumericEnergyForcesHF(self,atomlist,**kwargs):
        return self.RHFEnergyFunction(atomlist,**kwargs),self.NumericForces(atomlist, self.RHFEnergyFunction)

    def NumericForces(self,atomlist,EnergyFunction):
        import copy
        "Return the forces on each atom in atomlist via finite differences"
        Forces = []
        dx = 1e-4
        nat = len(atomlist)
        for i in xrange(nat):
            plus_x_geo = self.shift_geo(copy.deepcopy(atomlist),i,0,dx)
            minus_x_geo = self.shift_geo(copy.deepcopy(atomlist),i,0,-dx)
            plus_y_geo = self.shift_geo(copy.deepcopy(atomlist),i,1,dx)
            minus_y_geo = self.shift_geo(copy.deepcopy(atomlist),i,1,-dx)
            plus_z_geo = self.shift_geo(copy.deepcopy(atomlist),i,2,dx)
            minus_z_geo = self.shift_geo(copy.deepcopy(atomlist),i,2,-dx)
            fx = (EnergyFunction(plus_x_geo)-EnergyFunction(minus_x_geo))/(2*dx)
            fy = (EnergyFunction(plus_y_geo)-EnergyFunction(minus_y_geo))/(2*dx)
            fz = (EnergyFunction(plus_z_geo)-EnergyFunction(minus_z_geo))/(2*dx)
            Forces.append((fx,fy,fz))
        return array(Forces)

    def Frms(F):
        "Compute the RMS value of a vector of forces"
        sqsum = 0
        for fx,fy,fz in F: sqsum += fx*fx+fy*fy+fz*fz
        return sqrt(sqsum/len(F))

    # I don't know why but the NumericEnergyForcesHF function does not 
    # work well if I call it directly

    def forces_and_gradients(self, atomlist, EnergyForces=NumericEnergyForcesHF):
        E,F = self.NumericEnergyForcesHF(atomlist)
        # I said array but it is a list, and it doesn't work
        Farray = []
        for i in range(len(F)):
            Farray.append(F[i])
        # Here the list is converted as a numpy array
        Farray = np.array(Farray)
        #Farray = np.array(Farray, dtype=float).ravel()
        # but taking a look to the source, it must be included in dict
        #print "Farray", Farray
        return E, F

    # MMTK Stuff provided by K. Hinsen

    # This method is called for every single energy evaluation, so make
    # it as efficient as possible. The parameters do_gradients and
    # do_force_constants are flags that indicate if gradients and/or
    # force constants are requested.
    def evaluate(self, configuration, do_gradients, do_force_constants):
        
        #E, F = self.NumericEnergyForcesHF(self.atoms_quante)
        #atoms_quante = []
        #self.atoms_quante = atoms_quante
        #E,F = EnergyForces(self.atoms_quante)
        ##E = self.RHFEnergyFunction(self.atoms_quante)
        ##F = self.NumericForces(self.atoms_quante,E)
        #print 'before ask forces and gradients'
        E,F = self.forces_and_gradients(self.atoms_quante)
        #print 'after forces and gradients'
        #Farray = []
        #Farray2 = []
        #Fa = array([])
        results = {}
        results['energy'] = E

        #Gradients from ElectricField.py
        print self.universe
        if do_gradients:
            gradients = ParticleVector(self.universe)
            #gradients = Vector(0,0,0)
            for i in range(len(F)-1):
                gradients[i] = Vector(F[i])
                print 'vector array', Vector(F[i]).array

            results['gradients'] = gradients
        if do_force_constants:
            force_constants = SymmetricPairTensor(self.universe)
            results['force_constants'] = force_constants
        return results

class QuanteForceField(ForceField):

    """
    Quante
    """

    def __init__(self, atoms_quante):

        """
        @parameters atoms_quante: list of atoms provided in quante style
        """
        # Store arguments that recreate the force field from a pickled
        # universe or from a trajectory.

#	stuff = []
#	stuff.append(metal)
#	for i in range(len(ligands)):
#		stuff.append(ligands[i])
        #indices = self.getAtomParameterIndices([metal] + ligands)
        self.arguments = (atoms_quante)
        # Initialize the ForceField class, giving a name to this one.
        ForceField.__init__(self, 'quante')
        # Store the parameters for later use.
        #self.indices = indices
        self.atoms_quante = atoms_quante    

    # The following method is called by the energy evaluation engine
    # to inquire if this force field term has all the parameters it
    # requires. This is necessary for interdependent force field
    # terms. In our case, we just say "yes" immediately.
    def ready(self, global_data):
        return True

    # The following method is called by the energy evaluation engine
    # to obtain a list of the low-level evaluator objects (the C routines)
    # that handle the calculations.
    def evaluatorTerms(self, universe, subset1, subset2, global_data):
        # The energy for subsets is defined as consisting only
        # of interactions within that subset, so the contribution
        # of an external field is zero. Therefore we just return
        # an empty list of energy terms.
        if subset1 is not None or subset2 is not None:
            raise ValueError("Not yet implemented")
        # Here we pass all the parameters to
        # the Cython code that handles energy calculations.
        return [ QuanteTerm (universe,
#                         self.atoms_quante), numpy.int32]
                         self.atoms_quante),]
