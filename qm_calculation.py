import PyQuante
from PyQuante import Molecule, SCF, configure_output

from chimera import Point

configure_output(stream=sys.stderr)
at= []
atomList = []
molecule = None
molecule = chimera.openModels.list(id=0,modelTypes=[(chimera.Molecule)])[0]


for atoms in molecule.atoms:
        array=(atoms.element.number, Point(atoms.xformCoord()))
        at.append(array)

print at

mol=Molecule('kk',at)

solver=SCF(mol, method='HF',basis="sto-3g")
solver.iterate()

print "Result :", solver.energy
