"""\
 Minimizers.py: Geometry Minimizers

 This program is part of the PyQuante quantum chemistry program suite.

 Copyright (c) 2004, Richard P. Muller. All Rights Reserved. 

 PyQuante version 1.2 and later is covered by the modified BSD
 license. Please see the file LICENSE that is part of this
 distribution. 
"""

# Todo list:
# * conjugate gradient geometry minimizer
# * numerical hessian/frequencies

import copy
from math import sqrt
import PyQuante
from PyQuante import hartree_fock
from PyQuante.NumWrap import array
from chimera import Point

def shift_geo(atomlist,atom,dir,amount):
    atomlist[atom].r[dir] += amount
    return atomlist

def RHFEnergyFunction(atomlist,**kwargs):
    from PyQuante.hartree_fock import rhf
    return rhf(atomlist,**kwargs)[0]

def NumericEnergyForcesHF(atomlist,**kwargs):
    return RHFEnergyFunction(atomlist,**kwargs),NumericForces(atomlist,RHFEnergyFunction)

def NumericForces(atomlist,EnergyFunction):
    "Return the forces on each atom in atomlist via finite differences"
    Forces = []
    dx = 1e-4
    nat = len(atomlist)
    for i in xrange(nat):
        plus_x_geo = shift_geo(copy.deepcopy(atomlist),i,0,dx)
        minus_x_geo = shift_geo(copy.deepcopy(atomlist),i,0,-dx)
        plus_y_geo = shift_geo(copy.deepcopy(atomlist),i,1,dx)
        minus_y_geo = shift_geo(copy.deepcopy(atomlist),i,1,-dx)
        plus_z_geo = shift_geo(copy.deepcopy(atomlist),i,2,dx)
        minus_z_geo = shift_geo(copy.deepcopy(atomlist),i,2,-dx)
        fx = (EnergyFunction(plus_x_geo)-EnergyFunction(minus_x_geo))/(2*dx)
        fy = (EnergyFunction(plus_y_geo)-EnergyFunction(minus_y_geo))/(2*dx)
        fz = (EnergyFunction(plus_z_geo)-EnergyFunction(minus_z_geo))/(2*dx)
        Forces.append((fx,fy,fz))
    return array(Forces)

def Frms(F):
    "Compute the RMS value of a vector of forces"
    sqsum = 0
    for fx,fy,fz in F: sqsum += fx*fx+fy*fy+fz*fz
    return sqrt(sqsum/len(F))

def SteepestDescent(atomlist, EnergyForces=NumericEnergyForcesHF):
    "Called with a pointer to an energy/force evaluator"
    step = 0.1 # this seems awfully small
    Eold = None
    for i in xrange(50):
        E,F = NumericEnergyForcesHF(atomlist)
        print F, 'Forces, to see the format'
        print i,E,Frms(F),step
        for j in xrange(len(atomlist)):
            atomlist[j].r -= step*F[j]
        if Eold:
            dE = E-Eold
            if abs(dE) < 0.001: break
            if dE > 0:
                step *= 0.5
            else:
                step *= 1.2
        Eold = E
        #updateGeometry(atomlist)
    return atomlist

def NewtonRaphson(atomlist, EnergyForces=NumericEnergyForcesHF):
    "Invent"
    step=0.1
    Eold=None

    for i in xrange(50):
        E,F = EnergyForces(atomlist)
        print i,E,Frms(F),step
        for j in xrange(len(atomlist)):
            atomlist[j].r = ((atomlist[j].r + step*F[j])-(atomlist[j].r + F[j]))/step*F[j]
        if Eold:
            dE = E-Eold
            if abs(dE) < 0.00001: break
            if dE > 0:
                step *= 0.5
            else:
                step *= 1.2
        Eold = E
    return atomlist

def test():
    from PyQuante import Molecule
    h2 = Molecule('H2',
                  [(1,  (0.00000000,     0.00000000,     0.5)),
                   (1,  (0.00000000,     0.00000000,    -0.5))],
                  units='Angstrom')
    h2opt = SteepestDescent(h2)
    #h2opt = NewtonRaphson(h2)
    print h2opt
    return

def MinimizerChimera(molecule):
    from PyQuante import Molecule
    atom_list = []

    for atoms in molecule.atoms:
        array = (atoms.element.number, Point(atoms.xformCoord()))
        atom_list.append(array)
        mol=Molecule('kk',atom_list)
    print atom_list

    opt = SteepestDescent(mol)
    updated = []
    initial = []
    for i in range(len(opt.atoms)):
       updated.append(opt.atoms[i].r[0])
       updated.append(opt.atoms[i].r[0])
       updated.append(opt.atoms[i].r[0])

    return updated

def updateGeometry():
    molecule = chimera.openModels.list(id=0, modelTypes=[(chimera.Molecule)])[0]
    atoms = molecule.atoms[0]
    new_geom = MinimizerChimera(molecule)

    i=0
    while i < len(new_geom):
        atoms.setCoord(chimera.Point(new_geom[i],new_geom[i+1],new_geom[i+2]))
        i=i+3
    print 'Done' 
        

        
if __name__ == '__main__': updateGeometry()


